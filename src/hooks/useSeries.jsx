import { useContext } from "react"
import SeriresContext from "../context/SeriesProvider"

const useSeries = () => {
  return useContext(SeriresContext)
}

export default useSeries