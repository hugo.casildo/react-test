import { useParams } from "react-router-dom"
import useSeries from '../../hooks/useSeries'
import './Detail.css'

const Detail = () => {
  const { selectSerie, serieDetail } = useSeries()
  let { id } = useParams()
  selectSerie(id)
  const {
    name,
    image,
    language,
    summary,
    status
  } = serieDetail
  
  return (
    <div className="Detail container-fluid w-100 h-100 text-light">
      <div>
        <h1 className="pb-3">{name}</h1>
        <h3 className="pb-2">Lenguage: <span>{language}</span></h3>
        <h3 className="pb-2 ">Status: {status}</h3>
        <div dangerouslySetInnerHTML={{ __html: summary }} />
      </div>
      <figure>
        <img className="poster" src={image?.original} alt="name" />
      </figure>
    </div>
  )
}
export default Detail