import { useNavigate } from "react-router-dom";

import CardSerie from '../organisms/CardSerie'
import useSeries from '../../hooks/useSeries'
import SPagination from '../organisms/SPagination'
import SNavar from '../organisms/SNavBar'
import './Home.css'

const Home = () => {
  const {
    seriesToShow,
    totalPages,
    currentPage,
    increasePage,
    decreasePage,
   } = useSeries()

  const navigate = useNavigate()

  return (
    <>
      <SNavar />
      <div className='Series d-flex flex-wrap' >
        {
          seriesToShow.map(serie => (
            <div
              className="container--card col-12 col-sm-6 col-md-4 col-lg-2"
              onClick={() => navigate(`/detail/${serie.id}`) }
              key={`container${serie.id}`}
              >
              <CardSerie
                data={serie}
              />
            </div>
          ))
        }
      </div>
      {totalPages > 0 &&
        <SPagination
          currentPage={currentPage}
          increasePage={increasePage}
          decreasePage={decreasePage}
        />
      }
    </>
  )
}
export default Home