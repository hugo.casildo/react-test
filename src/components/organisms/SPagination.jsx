import { Pagination, PaginationItem, PaginationLink } from "reactstrap"
import './SPagination.css'
const SPagination = ({currentPage, increasePage, decreasePage}) => {
  
  return (
    <Pagination className="d-flex container--nav--center mt-2">
      <PaginationItem>
        <PaginationLink
          onClick={() => decreasePage()}
          previous
        />
      </PaginationItem>
      <PaginationItem>
        <div className="item d-flex">
          {currentPage}
        </div>
      </PaginationItem>
      <PaginationItem>
        <PaginationLink
          onClick={() => increasePage()}
          next
        />
      </PaginationItem>
    </Pagination>
  )
}

export default SPagination