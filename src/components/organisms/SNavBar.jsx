import { Navbar, NavbarBrand, NavItem, NavLink } from "reactstrap"
import './SNavBar.css'

const SNavBar = () => {
  return (
    <Navbar
      color="#000000"
      expand="md"
      light
    >
      <NavbarBrand href="/">
        <img className="nav-logo" src="https://upload.wikimedia.org/wikipedia/commons/f/ff/Netflix-new-icon.png" alt="Hugo Casildo" />
      </NavbarBrand>
      <NavItem>
        <NavLink href="/components/">
          Series
        </NavLink>
      </NavItem>

    </Navbar>
  )
}

export default SNavBar