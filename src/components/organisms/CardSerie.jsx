import { Card, CardImg, CardTitle, CardText, CardBody } from "reactstrap"
import parse from 'html-react-parser';
import './CardSerie.css'

const CardSerie = ({data}) => {
  const { name, image, summary} = data
  const parseSumary = (item) => parse(item)
  
  return (
    <Card className="mb-2">
      <CardImg
        alt="Card image cap"
        src={image.medium}
        top
        width="100%"
      />
      <CardBody>
        <CardTitle tag="h5">
          {name}
        </CardTitle>
        <CardText>
          {parseSumary(summary)}
        </CardText>
      </CardBody>
    </Card>
  )
}
export default CardSerie