import axios from 'axios'
const URL = process.env.REACT_APP_URL_BASE
const api = axios.create()

export const fetchAllSeries = () => {
  return api.get(`${URL}/shows`)
    .then(succ => succ.data )
}

export const getSerieDetail = (id) => {
  return api.get(`${URL}/shows/${id}`)
    .then(succ => succ.data )
}