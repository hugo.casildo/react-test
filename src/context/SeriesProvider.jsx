import { createContext, useEffect, useState } from "react"
import { fetchAllSeries, getSerieDetail } from "../core/api";

const SeriresContext = createContext();

const SeriesProvider = ({children}) => {
  const seriesPerPage = 12;
  const [series, setSeries] = useState([])
  
  const [currentPage, setCurrentPage] = useState(0)
  const [seriesToShow, setSeriesToShow] = useState([])
  const [totalPages, setTotalPage] = useState(0)
  const [serieSelected, setSerieSelected] = useState(0)
  const [serieDetail, setSerieDetail] = useState({})

  const getSeriesPerPage = () => {
    const initial = (currentPage - 1 ) * seriesPerPage
    const end = (currentPage ) * seriesPerPage
    return series.slice(initial, end)
  } 

  const calculateNoPages = (size) => {
    let pagination = size / seriesPerPage
    let remaning = size % seriesPerPage
    return remaning === 0
      ? pagination
      : Math.floor(pagination) + 1
  }

  useEffect(() =>{
    fetchAllSeries()
      .then(succ => {
        setSeries(succ)
        setCurrentPage(succ.length > 0 ? 1 : 0)
        setTotalPage(calculateNoPages(succ.length))
      })
  },[])

  useEffect(() => {
    setSeriesToShow(getSeriesPerPage())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage])

  useEffect(() => {
    serieSelected !== 0 && getSerieDetail(serieSelected)
      .then(succ=> {
        setSerieDetail(succ)
      })
  }, [serieSelected])

  const selectSerie = (id) => {
    setSerieSelected(id)
  }
  return (
    <SeriresContext.Provider
      value={{
        seriesToShow,
        totalPages,
        currentPage,
        serieDetail,
        selectSerie,
        increasePage: () => setCurrentPage(currentPage < totalPages ? currentPage + 1 : currentPage),
        decreasePage: () => setCurrentPage(currentPage > 1 ? currentPage - 1 : currentPage),
      }}
    >
      {children}
    </SeriresContext.Provider>
  )
}

export {
  SeriesProvider
}
export default SeriresContext