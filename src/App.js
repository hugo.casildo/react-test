import { Route, Routes } from 'react-router-dom'
import Home from './components/views/Home';
import Detail from './components/views/Detail';
import { SeriesProvider } from './context/SeriesProvider';


function App() {
  return (
    <div className="w-100 h-100">
      <SeriesProvider>
        <Routes>
          <Route exact path="/" element={<Home />}/>
          <Route exact path="/detail/:id" element={<Detail />}/>
        </Routes>
      </SeriesProvider>
    </div>
  );
}

export default App;
